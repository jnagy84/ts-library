Calendar Events have not rolled for over for 60 days

<p>We have a new script that we can run in version 6 to rollover events that are older than 60 days that have not rolled over.&nbsp;</p>

<p>It's called RolloverOlderEvents.sql. This script needs to be run by Blair or Development as it&rsquo;s a little more involved than most of our scripts.</p>