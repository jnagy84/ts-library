v8 DCU message: You have lost your Internet connection or your workstation work station is being blo

<p>. . . from connecting to your database. This block can be caused by antivirus and/or network security settings. Please contact your network administrator to check antivirus / firewall settings. DCU cannot connect.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>1. Check services. &nbsp;<span style="color: #0000ff;">Make sure that SQL Server Browser is set to "Local System account" not "This Account."</span></p>
<p>2. If SINGLE USER, RECREATE AAUSER AND RESTART INSTANCE (see Note under step 3).</p>
<ul>
<li>Run services.msc Services</li>
<li>right-click SQLserver(ADVISORSASSIST), Properties, Log on tab, put bullet on LOCAL SYSTEM ACCOUNT.</li>
<li>Restart service</li>
</ul>
<p>2. &nbsp;Run the DCU as Admin AND switch the authentication</p>
<p>3. If the connection has been successful previously and suddenly it no longer works, try IP address in the DCU, then try IP\ADVISORSASSIST</p>
<p>NOTE: aauser might need to be recreated (run script) from S:\Create AAUser <span style="color: #ff0000;">AND</span>&nbsp;if you run a script for aauser or rebuild it manually, restart the instance (right-click ADVISORSASSIST instance, click Restart)</p>
<p>4. If Windows Firewall is on, try turning it off, at the work station, in the Windows Firewall "add program" and browse to Advisors Assistant. Then click Add Program again and browse to the Database Connection Utility. Also disable antivirus. If AVG free, you have to uninstall (disable doesn't work with Free AVG); let client know that they need to reinstall AVG, consider getting version that has support.</p>
<p>5. AT the server, if no Advisors Assistant exceptions are listed, use Windows Explorer to browse to C:\Program Files\Client Marketing Systems\Advisors Assistant\Server Utilities\AAFirewallTool.exe. double-click to install. Hopefully, the Advisors Assistant Exceptions are listed. Verify exceptions below. If you have to add the exceptions manually, also manually add the port below:</p>
<ul>
<li>At the server, add ports 8900, 1434, 1433 all TCP (the 8900 can be at the server)</li>
<li>Name: Advisors Assistant Server Component Port number: 8900</li>
<li>&nbsp;SQL Browser Port number: 1434</li>
<li>&nbsp;SQL Service Port number: 1433 - sqlservr.exe (browse to C:\Program Files\Microsoft SQL Server\MSSQL.1 [or another MSSQL.x folder if multiple SQL dbs are on server]\MSSQL\Binn\sqlservr.exe OR IF SQL 2008 EX R2 browse to C:\Program Files\Microsoft SQL Server\MSSQL10_50.ADVISORSASSIST\MSSQL\Binn C:\Program Files\Microsoft SQL Server\90\Shared\sqlbrowser.exe</li>
<li>ALSO CHECK PINGING :check the server is visible from station; get server IP; MAKE SURE YOU CAN PING from station USING SERVER IP. If you can ping, use server IP in the DCU.</li>
</ul>
<p>NOTE: If you can ping the server, move on to the next stop. &nbsp;If you cannot ping the server, had them contact their IT--there's nothing else we can do for now.</p>
<p>6)&nbsp;<span style="color: #333333; font-family: Helvetica, sans-serif; font-size: 10.5pt; background-color: whitesmoke;">Microsoft SQL | SQL Server Configuration Manager. Use the drop-down on SQL Server Network Configuration and click on Protocols for ADVISORSASSIST. Make sure Named Pipes and TCP/IP are enabled. Right click on TCP/IP and choose properties. Click on the IP Address tab and make sure all IP&rsquo;s are enabled.</span></p>
<p><span style="color: #333333; font-family: Helvetica, sans-serif; font-size: 10.5pt; background-color: whitesmoke;">7. In Mgmt Studio check the database has tables</span></p>
<p><span style="color: #333333; font-family: Helvetica, sans-serif; font-size: 14px; background-color: #f5f5f5;">8) Uninstall and reinstall station</span></p>
<p>9) Check that SQL CLR Types are installed. &nbsp;When you drill down to O:\Tech Support\V8 SQL Components 10.5, all three refer to SQL 2008R2 in their name. They work just fine for SQL 2014. &nbsp;<span style="color: #0000ff;">NOTE: &nbsp;You must start by installing SQLSysClrTypes2008R2(x64 or x86).msi first, followed by SMO2008R2.msi and sqlncli2008R2.msi.</span></p>
<p style="padding-left: 60px;">NOTE: &nbsp;While installing these components you will be asked to enter in the company name. <span style="color: #0000ff;">This should be left blank</span>.</p>
<p>10) Rare cause: server and station on different subnets. You'll be able to see this when comparing the ipconfig info. The third segment in from the left is the subnet., e.g., in 192.18.1 the 1 is the subnet. If another pc has 192.18.10, it's on a different subnet.) &nbsp;<span style="color: #ff0000;">REFER TO THEIR IT</span></p>
<p>&nbsp;</p>