Panda a/v

<h3>When trying to open AdvisorsAssistant.exe with Run As, the message displays "A file is attempting to run on your computer that may pose a security risk for your company.&nbsp; As a preventive measure, it will be prevented from running until we check that it is a 100% trusted application.&nbsp;&nbsp;</h3>
<h3>&nbsp;</h3>

<h3>The info below (O: Tim Messett 1/25/18) might help you get the user to contact their Panda Administrator.</h3>
<h3>- Nancy says the problem occurred after our update so she's sure we caused the problem</h3>
<h3>- I explained that it's her a/v program&nbsp;</h3>
<h3>- she called her IT; he stopped the service on her a/v</h3>
<h3>- her AA log in screen came up</h3>
<h3>- he then restarted the service</h3>
<h3>- I commented that that this isn't a good solution if she can't open AA after exiting</h3>
<h3>- he said Panda doesn't have us listed as an acceptable program and asked me to roll back our update!</h3>
<h3>- I told him that this isn't a good solution</h3>
<h3>- Nancy pointed out that everyone else in the office took the update and can open their program</h3>
<h3>- I suggested he add our exe in Panda's exclusions</h3>
<h3>- he said he needs to put us on hold and find out how</h3>
<h3>- when he came back on he said he needs to restart her pc, did that</h3>
<h3>- she opened AA</h3>
<h3>- she again asked why she's the only staff member with the problem</h3>
<h3>- he said that it's a Windows 10 issue</h3>
<h3>- I commented that we've been installing successfully to Windows 10 for a few years now and that the problem is in Panda, add the exclusion and everything should be fine</h3>
<h3>- he agreed and said he'd do that!</h3>
<h3>&nbsp;</h3>