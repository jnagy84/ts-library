V6 Laser App Instantiation failed

<h1>pending solution for shared forms library: I got a station working with Laser App. &nbsp;It's mapped to the forms library host. &nbsp;On 5/24/17, hoping it'll solve the problem, I'm mapping the station that doesn't get the AA data into Laser App. &nbsp;O:Brian Grodman</h1>
<p>&nbsp;</p>
<p>. . . waited 20 seconds System.Runtime.InteropServices.COMException Operation unavailable (Exception from HRESULT: 0x80401E3 (MK_E_UNAVAILABLE)) Unable to start LaserApp. Add this command line parameter to the call to AdvisorsAssistant.exe: laserappwait=n where n is the number of seconds to wait. (This call is saved in the shortcut to AA) "C:\Program Files\Client Marketing Systems\Advisors Assistant\AdvisorsAssistant.exe" laserappwait=120 NOTE: If Laser App forms are on the server, the AA command line can be modified (see below)</p>
<p>&nbsp;</p>
<p>LASER APP ENTERPRISE (FORMS LIBRARY on a shared host)</p>
<p>1) Laser App | Utilities | Program Settings. &nbsp;Current Form Directory needs to show the host location of forms</p>
<p>2) Laser App | Utilites | Network and Database | Connect and Change. &nbsp;Under CURRENT DATABASE OPTIONS; put the bullet on the middle option (connect to default (local). &nbsp;</p>
<p>3) Make sure that the Laser App data location ends with the folder "Tables"</p>

<h1>NOTE;&nbsp; if this doesn't work, make sure you didn't make a type in the Target path!</h1>
<h1>1) Right-click the AdvisorsAssistant icon</h1>
<h1>2) Click Properties</h1>
<h1>3) Go to the Target field</h1>
<h1>4) At the end of the target info, after the quotation mark, type one space and then type: LASERAPPWAIT=120</h1>
<p>--------------------------------</p>
<p>Do not email info below to user!</p>
<p>2) If user's Laser App isn't current, ask them to update.</p>
<p>3) if error still occurs, check for file interop.elas.dll (in AA directory) dated 10/8/06 or newer. IF FILE IS PRESENT and correctly dated, AA install is fine so give them Laser App's tech support number; there's a problem with their installation.</p>
<p>4) if the user doesn't want to mess with possible uninstall/reinstall of Laser App, a POSSIBLE WORKAROUND is to try opening Laser App FIRST.</p>
<p>&nbsp;</p>