Lower half of view screen is offset from the upper half of the screen

<p>Stephen has figured out how to recreate this, but it has no progrmaming solution.</p>

<h2>WORKAROUND:</h2>
<h2>1) Go to File | Preferences | General tab.</h2>
<h2>2) IF the View Screen Resizing is lready enabled, then close and reopen AA. The screen should be fine.</h2>
<h2>3) IF the "View Screen Resizing" checkbox for "Enabled" is not checked, check it, click Ok</h2>
<h2>4) Close and reopen AA</h2>
<h2>5) When you re-open AA, screen should be fine.</h2>