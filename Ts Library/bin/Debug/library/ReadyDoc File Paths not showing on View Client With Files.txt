ReadyDoc File Paths not showing on View Client With Files

<p>User has ReadyDoc installed and scans, but file paths are not showing in Advisors Assistant View Client With Files screen.</p>
<p>&nbsp;</p>
<ul>
<li>User might be launching the ReadyDoc capture module outside of Advisors Assistant, which doesn&rsquo;t allow documents to be linked to an Advisors Assistant account.</li>
<li>Instead it prompts the user to pick a location in ReadyDoc to save the files.</li>
</ul>

<p>If the user launches ReadyCapture from within Advisors Assistant by clicking on the Scan or Add Files buttons, it launches with the information from the current client associated with the capture session and handles the storage location auitomatically.</p>