Terminated policy is still showing after "Show Terminated" is unchecked

<h3>"Show Terminated" is unchecked but policy with termination date is still displayed on the view screen.</h3>

<h2>1) There is an additional coverage on the policy that is NOT terminated (even though the base coverage is terminated).</h2>
<h2>2) If the additional coverage should be terminated, use the Status field or the Termination date field in the additional coverage.</h2>
<h2>NOTE: Even though the policy has a termination date, the ACTIVE additional coverages will show under Insured and Beneficiary perspectives because those relationships are available at the coverage level. Active additional coverages will NOT show under the OWNER perspective because that is a policy-level relationship.</h2>