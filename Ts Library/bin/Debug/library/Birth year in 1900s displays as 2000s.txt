Birth year in 1900s displays as 2000s

<p>Any birthday which includes a year in the 1900 to 1930 displays as 20XX.</p>

<p>This is a windows Setting. &nbsp;</p>
<p>Go to Control Panel | Region and Language.</p>
<ul>
<li>On Formats tab, click "Additional settings</li>
<li>Go to Date tab</li>
<ul>
<li>Under Calendar, "When a two-digit year is entered, interpret it as a year between 2025</li>
</ul>
</ul>