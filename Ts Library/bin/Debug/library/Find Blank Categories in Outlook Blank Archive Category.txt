Find Blank Categories in Outlook Blank Archive Category

<p>User believes that some emails were not flagged as not found in db or archived.&nbsp;&nbsp;</p>

<p>Rather than do a time-consuming lookback, you can search Outlook for category = empty.</p>
<p>1) Ctrl+Shift+F for Advanced Find.</p>
<p>2) Click Advanced tab.</p>
<p>3) Click Field down arrow, point to All Mail Fields and click on Categories</p>
<p>4) In the Condition field, choose "Is Empty"</p>
<p>5) click Add to List</p>
<p>If you need to control the date range:</p>
<p>1) click Field, point to Date/Time, click Received</p>
<p>2) In the Condition field, scroll to On or After and then in the Value field enter the appropriate date</p>
<p>3) Click Add to List</p>
<p>4) Repeat for "Sent"</p>
<h2><strong>Once you have all listed that you want to control you search, click Find Now</strong></h2>
<p>click Find Now.</p>
<p>&nbsp;</p>