A problem was encountered while trying to update the selected database

<p>There was a problem applying the Template to the selected database: Specific Failures Running the Synchronization.</p>
<p>or</p>
<p>"A problem was encountered while trying to update the selected database: &nbsp;There was a problem setting up the template that the Update uses. &nbsp;Please reinstall the server utilities or contact Technical Support. &nbsp;The database 'AAMasterTemplate' cannot be opened because it is version 661. &nbsp;This server support version 655 and earlier. &nbsp;A downgrade path is not supported. &nbsp;Could not open new database 'AAMasterTemplate'. &nbsp;CREATE DATABASE is aborted."</p>
<p>&nbsp;</p>

<p>Someone was in the db during the update</p>
<p>or</p>
<p>it's a text catalog problem (copy mdf and ldf, update WITHOUT backup usually brings the text catalog back online)</p>
<p>or</p>
<p>there's not enough room on C for the template to open</p>
<p>or <span style="color: #0000ff;">SQL 2005 Ex needs to be upated to SQL 2008 Ex R2 or higher</span></p>
<p>or</p>
<p>the database is corrupt or it's Small Business Server 2003 (this OS requires the exact opposite of everyone out: just make sure someone is IN the program) or the database needs to be updated here (bring it here and update it, then return the db)</p>
<p>&nbsp;</p>
<p>NOTE: If error mentions file group size problem, update SQL 2005 Ex to SQL 2008 Ex R2 (RO: Peter HIll) Go to the end of the error, read up and look for a mention of the full text catalog. 1) If the full text catalog is cited in the error, go to the TSL entry for dropping that catalog "v6 Drop Full Text Catalog." Follow the steps to drop the catalog. If not, try the sp_notecatalogdrop script. Before doing step 3, verify that there actually is enough room on C (double the size of the mdf) 2) If it's the 'not enough room' issue, open the Mgmt Studio, and look DIRECTLY under "Databases" (don't drill down past Databases) for a Master Template; if it's there, delete it. Then go to C:\Temp and if the Master Template is listed there, delete it, then start the update process again 3) if db is corrupt, the error message will include "comparing databases" and then it's multiple problems 4) if it's Small Business Server 2003, the opposite thing is needed: someone has to be in the program during the update. (You can also open SMS and ran: execute sp_dboption 'advisorsassistant','autoclose','off' but making sure someone is in the program is MUCH easier and faster) exec sp_AllCatalogDrop Go exec sp_NoteCatalogDrop Go</p>