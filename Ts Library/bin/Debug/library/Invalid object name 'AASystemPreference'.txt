Invalid object name 'AASystemPreference'

<p>If db and program version are NOT mismatched, see below.</p>
<p>If mismatched, update/downgrade to compatible versions.</p>
<p>Trying to open using program update 17.053 BUT database version was 16.053!!!</p>

<h2>If db and program are not mismatched, then the db needs a repair.</h2>