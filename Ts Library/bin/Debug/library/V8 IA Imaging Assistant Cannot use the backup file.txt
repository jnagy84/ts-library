V8 IA Imaging Assistant Cannot use the backup file

<p>User tries to make IA backup and gets message "Cannot use the backup file 'C. . . .bak' because it was originally formatted with sector size 512 and is now on a device withy sector size 4096. &nbsp;</p>
<p>BACKUP DATABASE is terminating abnormally.</p>

<p>1) The backup routine is finding a previous backup in the folder.</p>
<p>2) Rename the DatabaseBackup folder&nbsp;</p>
<p>3) Launch the backup again.</p>
<p>4) When you name the file, the backup routine will create a new DatabaseBackup folder (which is empty) so no previous backup will be bumped into.</p>