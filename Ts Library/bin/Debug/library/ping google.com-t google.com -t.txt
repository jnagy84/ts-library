ping google.com-t google.com -t

<p>Internet checker results don't show a disconnect, IT says the Internet connection problem is not on their end.</p>

<p>1) Run CMD.</p>
<p>2) In the command line, type: ping google.com -t (there is a space before -t)</p>
<p>The above ping command will ping Google approximately every second until you close the command prompt or use the keyboard shortcut Ctrl+C. To stop the ping and view results of the ping, hit Ctrl+C</p>
<p>3) you can show user where their Internet packets are bogging down by running CMD: tracert climark.com</p>