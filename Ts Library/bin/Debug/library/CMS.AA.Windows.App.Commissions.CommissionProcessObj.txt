CMS.AA.Windows.App.Commissions.CommissionProcessObj

<h3>baseAmountChanges : PostTransactionBaseAmountChangeCollection</h3>

<h3>User was a posting session and had clicked the "Pay In Full" button for a specific transaction. The problem is likely data-related, and if that's the case the other transactions may post just fine... GTA with Nate or if web-based, he'll need to connect, and examine the transaction.</h3>
<h3>&nbsp;</h3>
<h3>O: John Hohman 5/24/13</h3>