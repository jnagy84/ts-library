Set Permissions for Shared Folders

<p>Tim Ferris sent info below on how to set permissions for users who connect to a shared folder over the network.&nbsp; &nbsp; Share permissions do not affect users who log on locally or log on using Remote Desktop.</p>

<p>1) Control Panel | Admin Tool</p>
<p>2) Open Computer Management</p>
<p>3) If the User Account Control dialog box appears, confirm that the action it displays is what you want, and then click Yes.</p>
<p>4) In the console tree, click System Tools, click Shared Folders, click Shares.</p>