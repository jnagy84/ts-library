Pre-login handshake error

<p>"Connection Timeout Expired. The timeout period elapsed while attempting to consume the pre-login handshake acknowledgement."</p>
<p>The wireless or local area connection properties are not configured for BOTH IPv4 and IPv6. The fact that SSMS worked turned out to be coincidental (the first few attempts presumably used IPv4). Some later attempts to connect through SSMS resulted in the same error message. Apparently some connection attempts ended up using IPv4 and others used IPv6.</p>

<p>1) You can Google http://stackoverflow.com/questions/15488922/connection-to-sql-server-works-sometimes</p>
<p>2) The SQL install CMS uses covers the enabling of IP BUT the workstation or single user host can still need changes to the Wireless or LAN connection properties:</p>
<ul>
<ul>
<li>In Control Panel, open network and sharing</li>
<li>Click on the connection being used</li>
<li>Click Properties | Networking tab</li>
<li>Click the checkbox for "Internet Protocol Version 6 (TCP/IPv6)</li>
</ul>
</ul>