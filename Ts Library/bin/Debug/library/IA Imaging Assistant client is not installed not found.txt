IA Imaging Assistant client is not installed not found

<p>If user's Security Role doesn't include IA module, and imaging assistant functions, instead of a notice displaying that "Your System Administrator has not granted . . ." user gets message: IMAGING ASSISTANT CLIENT IS NOT INSTALLED.</p>
<p>&nbsp;</p>
<p>NOTE: If the IA record is not in the iKEYMODULE column in the AALicenseModule table, you'll have to re-register Advisors assistant (and modules)</p>
<ul>
<li>If Imaging Assistant is not used, e.g., user uses ReadyDoc, remove the IA record (1023) from the iKEYMODULE column in the AALicenseModule table.</li>
</ul>

<p>This message does appear legitimately when Imaging Assistant Client is not installed.</p>
<p>1) Check Add/Remove Programs in the Control Panel and see if it is really not there.</p>
<p>2) Further confirmation would also be to go to C:\Program Files (x86)\Client Marketing Systems\Imaging Assistant.</p>
<ul>
<li>If there are no files in that folder or the folder does not exist, then it is not installed and it needs to be installed first.</li>
<li>If Imaging Assistant IS installed, then it is a false message and the module is possibly not activated or the user does not have the security right to use Imaging Assistant.</li>
<li>If Imaging Assistant is not used, e.g., user uses ReadyDoc, remove the IA record (1023) from the iKEYMODULE column in the AALicenseModule table.</li>
</ul>
<p>NOTE: &nbsp;There are TWO security tasks for IA: the Add/Modify Scanned Files AND access to the Optional Module IA</p>
<p>NOTE: DON'T CLIMARK on a station WITH IA installed, it will activate IA again.</p>