Microsoft Fixit for Windows 7, 8 and 8.1

<p>There can be corrupted registry keys. &nbsp;The corrupted registry keys can do the following:</p>
<ul>
<ul>
<li>cause problems that prevent new programs from installing</li>
<li>cause problems that prevent existing programs from being completely uninstalled or updated</li>
<li>cause problems that block you from uninstalling a program through the Add or Remove Programs (or Programs and Features) item in Control Panel</li>
</ul>
</ul>

<h3>1) Microsoft developed a "Fixit" tool to repair issues that block program installation or removal because of corrupted registry keys.</h3>
<h3>2) Instructions and links for Windows 7, 8.0 and 8.1 are at O:\Tech Support\</h3>
<ul>
<ul>
<li>
<h3>There are separate folders</h3>
</li>
<ul>
<li>
<h3>V8 Microsoft V8 Microsoft Fixit for Win8 and 8.1 &nbsp;</h3>
</li>
<li>
<h3>V8 Microsoft Fixit Win7 and earlier</h3>
</li>
</ul>
</ul>
</ul>
<p>3)&nbsp;<span style="font-size: 0.9375rem; font-family: 'Segoe UI', 'Segoe UI Web', 'Segoe UI Symbol', 'Helvetica Neue', 'BBAlpha Sans', 'S60 Sans', Arial, sans-serif;">Fix-it tools aren&rsquo;t around anymore in Windows 10. Instead, use a troubleshooter to help solve problems with your PC.</span></p>
<div class="col-xs-24" style="box-sizing: border-box; outline: none; position: relative; min-height: 1px; padding-left: 24px; padding-right: 24px; float: left; width: 757.781px; font-family: 'Segoe UI', 'Segoe UI Web', 'Segoe UI Symbol', 'Helvetica Neue', 'BBAlpha Sans', 'S60 Sans', Arial, sans-serif; font-size: 15px;">
<ol class="ia-bullet ia-single-bullet" style="box-sizing: border-box; margin: 0px; list-style-type: none; padding-left: 0px;">
<li class="ng-scope" style="box-sizing: border-box; margin-bottom: 20px;"><span class="text-body ng-scope" style="box-sizing: border-box; outline: 0px; margin-top: 0px; margin-bottom: 0px; font-size: 0.9375rem; line-height: 1.33333;"><span class="ng-binding ng-scope" style="box-sizing: border-box; outline: none;">To find all of the troubleshooters, go to the search box and enter&nbsp;</span></span><span class="text-body ng-scope" style="box-sizing: border-box; outline: 0px; margin-top: 0px; margin-bottom: 0px; font-size: 0.9375rem; line-height: 1.33333;"><span class="ng-binding ng-scope" style="box-sizing: border-box; outline: none;"><span style="box-sizing: border-box; font-weight: 600;">Troubleshooting.</span></span>&nbsp;</span><span class="text-body ng-scope" style="box-sizing: border-box; outline: 0px; margin-top: 0px; margin-bottom: 0px; font-size: 0.9375rem; line-height: 1.33333;"><span class="ng-binding ng-scope" style="box-sizing: border-box; outline: none;">Open the</span></span><span class="text-body ng-scope" style="box-sizing: border-box; outline: 0px; margin-top: 0px; margin-bottom: 0px; font-size: 0.9375rem; line-height: 1.33333;"><span class="ng-binding ng-scope" style="box-sizing: border-box; outline: none;"><span style="box-sizing: border-box; font-weight: 600;">Troubleshooting&nbsp;</span></span></span><span class="text-body ng-scope" style="box-sizing: border-box; outline: 0px; margin-top: 0px; margin-bottom: 0px; font-size: 0.9375rem; line-height: 1.33333;"><span class="ng-binding ng-scope" style="box-sizing: border-box; outline: none;">Control Panel and select&nbsp;</span></span><span class="text-body ng-scope" style="box-sizing: border-box; outline: 0px; margin-top: 0px; margin-bottom: 0px; font-size: 0.9375rem; line-height: 1.33333;"><span class="ng-binding ng-scope" style="box-sizing: border-box; outline: none;"><span style="box-sizing: border-box; font-weight: 600;">View all&nbsp;</span></span></span><span class="text-body ng-scope" style="box-sizing: border-box; outline: 0px; margin-top: 0px; margin-bottom: 0px; font-size: 0.9375rem; line-height: 1.33333;"><span class="ng-binding ng-scope" style="box-sizing: border-box; outline: none;">from the left pane.</span></span></li>
</ol>
</div>