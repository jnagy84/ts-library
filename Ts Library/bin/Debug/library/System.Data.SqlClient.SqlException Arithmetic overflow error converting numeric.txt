System.Data.SqlClient.SqlException: Arithmetic overflow error converting numeric

<h3>System.Data.SqlClient.SqlException: Arithmetic overflow error converting numeric converting numeric to data type numeric.</h3>
<h3>&nbsp;</h3>
<h3>(This error occurred when a user modified any information on a client's investments tab that caused a recalculation AND they had the offending fund within their portfolio. We isolated the bad price that was causing the program to fail: 9/28/2007 Share Price = 7,835,998,450.000000 (yes, 7 billion and some change PER SHARE!) You can see that if the client has even a small number of shares at that price, it will quickly outgrow even the max Base64&gt;.)</h3>

<p>Run the script to locate the price: O:\Tech Support\V6 Scripts\Find Ridiculous Price (FindRidiculousPricing.sql) so that the price can be corrected. &nbsp;If the price is actually correct, this will need to be a vault item for development to accommodate the price.</p>