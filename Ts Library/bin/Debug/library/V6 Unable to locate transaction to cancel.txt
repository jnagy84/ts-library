V6 Unable to locate transaction to cancel

<p>Please review share balance for this investment.</p>

<p>Because the cancelling transaction was coded incorrectly, Glen cannot locate the transaction to cancel. &nbsp;If Glen were to honor the incorrect code, he says that all the people who received cancelling transactions coded correctly would have them reversed!</p>