The Installer has insufficient privileges to . . .

<p>The Installer has insufficient privileges to modify the file C:\Users\Public\Desktop\Advisors Assistant.lnk"</p>
<p>OR</p>
<p>The Install her insufficient privileges to access this directory: "C:\Config.msi"</p>
<p>&nbsp;</p>
<p>PICTURE OF ERROR IS AT&nbsp;O:\Tech Support\V7 Screens to Supplement TSL Entries\Station Install Error - Insufficient Privileges.jpeg</p>

<p>1) Keep Error open (or after the steps below you'll need to start the install again)</p>
<p>2) In Windows Explorer, right-click the folder, open Properties | Security tab</p>
<p>3) Grant "Full Control" to the role of 'Everyone'</p>
<p>4) Click 'Retry' on the above error; station finished installing or start the install over again</p>
<p>&nbsp;</p>
<p>if Config.msi is the issue, grant "full Control" to the role of Everyone on C:\Temp</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>