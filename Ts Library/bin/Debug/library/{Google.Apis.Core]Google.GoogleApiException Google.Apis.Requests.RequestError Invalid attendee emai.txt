{Google.Apis.Core]Google.GoogleApiException: Google.Apis.Requests.RequestError Invalid attendee emai

<p>[Google.Apis.Core]Google.GoogleApiException: Google.Apis.Requests.RequestError</p>
<p>Invalid attendee email</p>

<p class="MsoNormal" style="margin-bottom: 6.0pt;">Google is not liking an email address associated with an attendee--meaning a "with" name on a calendar event.</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">&nbsp;</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">see O: David Stombaugh 2/2/2017 for effectiveness of solution.</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">In this case, the event is 6/22/2017&nbsp;"Skin Check Up"</p>
<p>&nbsp;</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">The With name for this event has an email address "<a href="http://academydermatologists.gmail.com">academydermatologists.gmail.com</a>" <em>without an "@"</em>.&nbsp; Please have them fix this email address and try the calendar export to Google again.</p>