V8 Import List Logic

<p>Below are details of what does/doesn't import to an existing name.</p>

<p>1) Name type used in Import Wizard is different than the name type in a matching name = name type will be added.</p>
<p>2) Importing a name with a producer matching the existing record will add phone numbers, email addresses and physical addresses. &nbsp;</p>
<p>3) Importing changes in an email, phone number or address will add the changes; it will not overwrite exsiting email, phone number, address.</p>
<p>&nbsp;</p>