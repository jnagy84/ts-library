AAMobile Log in gets "session expired" message

<p>User can't log into AAMobile on a device. The login works on another device. Gets message "session expired"</p>

<p>Check system date on device. In this case, on 8/7/14 the sys date was set in the future: 10/6/14.</p>