v6 Email Archiving Timer Logic

<p>The timer triggered archiver can miss emails. It&rsquo;s important to understand how it operates.</p>

<p>1. It keeps track of the last date and time that it archived emails.</p>
<p>2. When Outlook is first started, 4 minutes after startup, it will do an archive and look back to midnight of the date of the last archive.</p>
<p>3. Subsequently, it will look back to the date and time of the last archive less about 15 minutes. That 15 minutes gives emails in transit to your server time to be included in the next check.</p>
<p>4. The date and time used is the date/time on the email. That could be when the email arrived at your server. Here&rsquo;s ONE way it can miss emails:</p>
<ul>
<ul>
<li>You start Outlook.</li>
<li>Email does not get checked. (Your Exchange may show some in your inbox, but we are not sure they are there until email is actually sent/received.)</li>
<li>The 4-minute timer triggers, and then a new time is recorded for the next check.</li>
<li>Emails are then received with a date/time prior to the date/time of the next check, so they are not included in the parameter. .</li>
<li>The next check does not pick them up.</li>
</ul>
</ul>