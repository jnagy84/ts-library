Archive Attachment with Email Archiving Off

<p>User only wants to archive using "Archive to Current Name Viewed."</p>

<p>CLOSE OUTLOOK.</p>
<p>1) In User Preferences, turn on email archiving.</p>
<p>2) in the Account field, enter a false email address, e.g., placeholder@aa.com</p>
<p>3) UNCHECK the Do Not Archive option.</p>
<p>4) Uncheck the incoming/outgoing email option.</p>
<p>5) Click OK.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>