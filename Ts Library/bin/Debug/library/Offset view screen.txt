Offset view screen

<p>Lower half of view screen is offset.</p>

<h3>1) Double-click on Perspectives.</h3>
<h3>If that doesn't work,</h3>
<h3>2) go to File | Preferences | General tab, View Screen Resizing and ENABLE it.</h3>