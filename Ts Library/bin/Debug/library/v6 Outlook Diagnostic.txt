v6 Outlook Diagnostic

<p>If user is having Outlook freezing issue, please see below note: in July 2012 for double-click screen change issue, right-click icon, choose Properties, in the target field add "track_click" to the shortcut's target field (with a space between .exe and track_click) e.g., "C:\Program Files\Client Marketing Systems\Advisors Assistant\AdvisorsAssistant.exe" track_click</p>

<p>Go to O:\Tech Support\V7 Outlook Diagnostic Tool SCRIPT TO TURN ON DIAGNOSTIC IS AT O:\TechSupport\V7 Script instructions: File is at O:\Tech Support\V7 Outlook Diagnostic</p>
<p>1) transfer DiagnosticModeON.reg to users&rsquo;s Desktop</p>
<p>2) close Outlook</p>
<p>3) execute the file</p>
<p>4) open Outlook</p>
<p>5) in AA run the sync process that is having the issue</p>
<p>6) close Outlook</p>
<p>7) GTA the file from C:\Users\(user name)\My Documents\Advisors Assistant\Outlook Diagnostic.txt</p>
<p>8) set a calendar event to SET THE VALUE TO 0 after the user has the problem solved (so that the text file isn't continuing to generate) - regedit and instructions are below. If O isn't available for some reason, you can create the reg edit: 1) Make sure db is at least 7.15.230.258.1 2) Check the Registry HKEY_CURRENT_USER |SOFTWARE | Client Marketing Systems Inc | Advisors Assistant | AAOutlookAddin (6 ot 7) 3) Right-click, choose Add New | String Value = isDiagnosticMode 4) Value = 1 5) Restart Outlook and run the process in AA that is having the issue 6) the text file generated will be at My Documents \Advisors Assistant\OutlookDiagnostic.txt 7) email to support@climark.com 8) set a calendar event to SET THE VALUE TO 0 after the user has the problem solved (so that the text file isn't continuing to generate)</p>