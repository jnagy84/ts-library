AAMobile Procedure or function sp_

<p>Most AAMobile errors start with Procedure or function sp_&nbsp;</p>

<p>ALWAYS start with asking a web-based staff person to run the appropriate script. &nbsp;It will never hurt, and might solve the problem.</p>
<p>If the error continues after the script is run, then it goes to development.</p>