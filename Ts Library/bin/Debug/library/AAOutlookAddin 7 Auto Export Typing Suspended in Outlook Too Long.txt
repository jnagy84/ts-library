AAOutlookAddin 7 Auto Export Typing Suspended in Outlook Too Long

<h3>The autoexport of calendar is taking too long, leaving the "Typing Suspended" warning up in Outlook for multiple minutes.</h3>

<h2>1) In Registry, go to HKey_Current_User\Software\Client Marketing System\Advisors Assistant\AAOutlookAddin7\"Last Auto Sync Start"</h2>
<h2>2) Modify to current date/time</h2>