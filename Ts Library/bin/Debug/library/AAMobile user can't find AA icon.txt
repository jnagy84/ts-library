AAMobile user can't find AA icon

<p>AAMobile is not an app that loads to the phone; it&rsquo;s a browser app, so browse to myaamobile.com and then from the login screen, create the bookmark and home screen icon the same way you would for any other web page.</p>

<p>Ask user to try browsing to myaamobile.com on their mobile device. If they get the Login Screen, then they just use their Advisors Assistant User ID and Password. If they get a Registration Screen, then they'll need a new Registration Link from us.</p>