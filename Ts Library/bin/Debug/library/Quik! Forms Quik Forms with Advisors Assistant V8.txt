Quik! Forms Quik Forms with Advisors Assistant V8

<p>Our mutual user can't get Quik! Forms to function from our sidebar FORMS icon.</p>

<p>1) In Advisors Assistant, go to File | User Preferences | Form Automation and select Quik! Forms</p>
<p>2) To connect the database:</p>
<p>3) Make sure the user is on Quik! Forms 8 (if on Quik! 7, there should be an update prompt)</p>
<p>4) In Advisors Assistant, click the Forms button and Quik Forms Library will launch. &nbsp;</p>
<p>Choose your forms and click Start</p>
<p>&nbsp;5) If the above doesn't work, call Quik! Forms support, ask for Jared Lawrence</p>
<p>Note: You will no longer see the clients in the dropdown under Step 1, instead a line in red at the top of the Quik screen will indicate that there is client data waiting. &nbsp;Just choose your form/s and click Start. &nbsp;The client data will populate on the form.&nbsp;</p>
<p>&nbsp;</p>
<p>To run more forms, just close Quik and find the client in AA, click the Forms button and the form will launch populated.</p>
<p>&nbsp;</p>