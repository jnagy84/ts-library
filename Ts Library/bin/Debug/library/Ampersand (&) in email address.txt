Ampersand (&) in email address

<p>Although technically, an ampersand is a valid character in an email address, Outlook's TO field will drop everything in the email address after the "&amp;" so this is NOT an Advisors Assistant issue.</p>

<p>To prove that this is not an AA issue, but is in fact an Outlook issue, show the user the following:</p>
<p>1) in a a new email message body in Outlook, type mailto:test@sally&amp;smith.com</p>
<p>2) Ctrl+Click on the email address</p>
<p>3) Nothing after the &amp; will populate in Outlook's TO field.</p>