System.data.Odbc.OdbcException ODBC Text Driver '(unknown)' is not a valid path.

<p>Occurred when trying to process a UNIFI file. CHECK ERROR FOR THE WORDS: Error text includes "Temporary (volatile) Jet DSN for process 0xc44 Thread 0x134 DBC 0xcf9a4c4 Text</p>

<h3>Test the UNIFI file by doing the following:</h3>
<h3>1) Copy the file to C:\Client Marketing Systems folder</h3>
<h3>2) Set security permissions to 'Everyone' with full control.</h3>
<h3>3)&nbsp;If the file processes successfuly in the above path, the issue is most likely permissions.</h3>
<h3>4) If the same error occurs, then the user has a corrupt or missing Microsoft ODBC text driver and it is his responsibility to replace/repair.</h3>
<h3>&nbsp;</h3>
<p>&nbsp;</p>
<p>O: Travis Catania)</p>