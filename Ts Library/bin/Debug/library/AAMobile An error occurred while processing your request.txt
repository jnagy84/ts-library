AAMobile An error occurred while processing your request

<p class="MsoNormal" style="margin-bottom: 6.0pt;">&nbsp;</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">1) User gets error when adding information to a name in AAMobile.</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">Error.</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">An error occurred while processing your request.</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">Details</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">Cannot cast DBNull.Value to type 'System.Int32."&nbsp; Please use a nullable type.</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">2) <strong>CAUSE:</strong> Name record does NOT have a primary name type filled in.</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">3) No primary name type can occur in AAMobile. 'None Assigned' is set as the default, but you can leave the field blank.</p>
<p>&nbsp;</p>
<p class="MsoNormal" style="margin-bottom: 6.0pt;">&nbsp;</p>

<p><strong>RESOLUTION:</strong> Fill in a primary name type on the name record.</p>