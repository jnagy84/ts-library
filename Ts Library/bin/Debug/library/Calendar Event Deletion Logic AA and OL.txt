Calendar Event Deletion Logic AA and OL

<p>Tech Support needs logic for how calendar events get deleted in AA and Ol. Mike Powers provided the info below, dated 1/9/15.</p>

<h3>Here&rsquo;s the logic:</h3>
<h3 style="padding-left: 30px;">&bull; Deleting an event from AA calendar, deletions of that event in AA are marked by setting iKeyLink of the event in the AAOutlookEntryCalendar table for the event for any Outlook Store that references that event.&nbsp;</h3>
<ul>
<li>If RealTime Export enabled, the deletion triggers a &lsquo;message&rsquo; being sent to Outlook, and the corresponding event in OL is deleted immediately from the OL on the machine from which the AA delete was done.</li>
<li>Deletions on any other machines synced to the calendar will have the event deleted the next time that OL is synced.</li>
<li>If AutoSync (the timer-triggered sync) is enabled, all events marked for deletion since the last sync in AAOutlookEntryCalendar for the Store on the machine being synced are deleted before processing any other sync info.</li>
<li>If a Manual sync is initiated by user, all events marked for deletion in AAOutlookEntryCalendar for the Store on the machine being synced are deleted before processing any other sync info. This occurs whether the sync is an export to OL or an import to AA.</li>
<li>Deleting an event from OL: deletion of that event from AA depends on the options.</li>
<li>If RealTime Import is enabled, the add-in handles the ItemAdded event in the &ldquo;Deleted&rdquo; folder by deleting the event in AA from the calendar that the OL is being synced with. It only deletes the event from that calendar, leaving the event itself in place, unless the event is only on the calendar being synced.</li>
<ul>
<li>Caveat 1: the add-in does its deletion only if the computer can reach the database; it makes no record that there is a &ldquo;pending&rdquo; change.</li>
<li>Caveat 2: there are times when (for whatever reason) OL does not fire the event that the add-in uses to know that something needs to be done to the event item. There is nothing that we can do to fix this (at this time).</li>
</ul>
<li>There is no AutoSync import. (Adding a timer-driven import would fix both the problems noted in the caveats above.)</li>
<li>During a Manual sync, the add-in checks entries in AAOutlookEntryCalendar for the store being synced. It searches the OL, and any entry where the EntryID saved therin cannot be found in OL has the reference to the event deleted from the calendar being synced &hellip; and deletes the event completely if it is only on the one calendar.</li>
</ul>