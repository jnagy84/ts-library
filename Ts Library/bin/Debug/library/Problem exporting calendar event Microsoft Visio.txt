Problem exporting calendar event Microsoft Visio

<p class="MsoNormal">User gets the following message when attempting to real-time sync an event created in AA to OL:</p>
<p class="MsoNormal"><span style="color: #0000ff;">"Problem exporting calendar event for xxx: Either Microsoft Outlook or Microsoft Windows is preventing Advisors Assistant from synching this calendar event into your Outlook calendar.&nbsp; If this keeps happening, you may need to close Outlook and then re-open it.&nbsp; If that does not fix the problem, then you should restart this computer."</span></p>
<p class="MsoNormal">&nbsp;</p>

<p class="MsoNormal">In most cases, &ldquo;The Works&rdquo; is necessary to correct this issue.&nbsp; <strong>HOWEVER, for this particular user, the issue was caused by multiple versions of Office installed on the machine.</strong> The workstation came with Office 365 (2016) pre-installed. The user prefers using Office 2010, so that is what he installed. His IT removed the Office 365 (2016) installation. HOWEVER, a remnant of the Office suite was left behind: Microsoft Visio Professional 2016, which is a flowchart and diagramming software program.</p>
<p class="MsoNormal">Confirm with the user that they have no need for Microsoft Visio Professional on the machine and uninstall it. Rebooted after uninstall. Real-time sync should work great afterwards.</p>
<p class="MsoNormal">&nbsp;</p>
<p><span style="font-size: 11.0pt; font-family: 'Calibri',sans-serif; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;"><!-- [if gte vml 1]><v:shapetype
 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
 <v:stroke joinstyle="miter"/>
 <v:formulas>
  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
  <v:f eqn="sum @0 1 0"/>
  <v:f eqn="sum 0 0 @1"/>
  <v:f eqn="prod @2 1 2"/>
  <v:f eqn="prod @3 21600 pixelWidth"/>
  <v:f eqn="prod @3 21600 pixelHeight"/>
  <v:f eqn="sum @0 0 1"/>
  <v:f eqn="prod @6 1 2"/>
  <v:f eqn="prod @7 21600 pixelWidth"/>
  <v:f eqn="sum @8 21600 0"/>
  <v:f eqn="prod @7 21600 pixelHeight"/>
  <v:f eqn="sum @10 21600 0"/>
 </v:formulas>
 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
 <o:lock v:ext="edit" aspectratio="t"/>
</v:shapetype><v:shape id="Picture_x0020_2" o:spid="_x0000_i1025" type="#_x0000_t75"
 alt="" style='width:780.75pt;height:302.25pt'>
 <v:imagedata src="file:///C:\Users\pam\AppData\Local\Temp\msohtmlclip1\01\clip_image001.png"
  o:href="cid:image002.png@01D4FB54.58228930"/>
</v:shape><![endif]--><!-- [if !vml]-->Screen shot at&nbsp;<img style="font-family: Calibri, sans-serif; font-size: 11pt;" src="file:///C:\Users\pam\AppData\Local\Temp\msohtmlclip1\01\clip_image002.gif" alt="" width="1041" height="403" /><span style="font-family: Calibri, sans-serif;"><span style="font-size: 14.6667px;">O:\Tech Support\V8 Screens to Supplement TSL Entries\Control Panel List of Programs showing Microsoft Visio.msg</span></span><!--[endif]--></span></p>