Vonage Vocalocity AutoDialer Auto Dialer Phone Dialer

<p>"There was an error trying to dial this number through Vocalocity. The remote server returned an error (400) Bad Request. Please Contact Technical Support."</p>
<p><span style="color: #0000ff;">The technical support, in this case, is Vonage. However, we can helpfully offer the info IN THE RESOLUTION SECTION below.</span></p>
<p>NOTE: &nbsp;We developed our Vonage connectivity with their API BEFORE Vonage had a plug-in.</p>
<p>- Jason at Vonage found us in the Vonage db as a partner under Client Marketing Systems.</p>
<p>- Vonage support might tell our user that there should be a plugin because that Vonage support person doesn't know our history of being an early Vocalocity adopter, --using their API before Vocalocity (later changed to Vonage) had a plug in!</p>
<p>(Vonage does have a "plug-in" which they developed for some of the big horrizontal market crm's like Sales Force and Sugar, but they also have the API for those who want to develop their own systems, such as us. &nbsp;They are going to set us up with Innovative Business Solutions, a 3rd party sales agent of theirs, which our users can call.)</p>

<h2>&nbsp;1) Each phone extension has its own phone number tied to a name (e.g. Terry) in the user's Vonage website.</h2>
<h2>&nbsp;2) User can log into www.My.Vonagebusiness.com.</h2>
<h2>&nbsp;3) At the top of the screen, click the "Phone System" icon.</h2>
<h2>&nbsp;4) Click on Extensions</h2>
<h2>&nbsp;5) Click on the user's extension number</h2>
<h2>&nbsp;6) Click on the User Name down arrow</h2>
<h2>&nbsp;7) Select the user name and select extension to link to that user's name</h2>
<h2>&nbsp;8) Click Save, Save change</h2>
<h2>&nbsp;9) Click on Users</h2>
<h2>10) Under the USER ID column, click the dname hyperlink</h2>
<h2>11) Click Default Extension and choose the extension</h2>
<h2>12) Click Save Changes</h2>
<h2>13) Log out of Vonage</h2>
<h2>User should now be able to use Dial function.</h2>
<h2>NOTE: If the user is dialing from a different phone than their AA Preferences | Other Passwords Vocalocity/Vonage login, the autodialer can't connect. (At a different phone, these credentials are different than the user name and password as a Vonage customer.) Once the phone extension user name and password are corrected in AA, the dialer will work.</h2>
<h2>&nbsp;</h2>
<h2>O: Jerry Neilsen</h2>