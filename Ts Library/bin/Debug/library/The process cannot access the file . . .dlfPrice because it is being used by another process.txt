The process cannot access the file . . .dlfPrice because it is being used by another process

<p>If station goes into screen saver, the user's login can be "locked" in SQL and so the file is in use.</p>

<p>Turn off the screen saver.</p>