CLIMARK to delete calendar events

<p>User needs to delete MANY calendar events, doesn't want to be limited to delete 100 at a time.</p>

<p>Use CLIMARK to delete as many events as you have displayed in the Reports view.&nbsp; Any recurring events will be deleted without the option "just this event" or "entire series."</p>