V6 Retrieving the COM class factory for component with CLSID (with Office Suite)

<p>Whenever COM, factory, component are cited, there are registry items missing.&nbsp;</p>
<p>&nbsp;</p>
<p>The details of the registry item are cited in the error in brackets). e.g., Retrieving. . {000209FF-0000-C000-000000000046}</p>

<ol>
<li>&nbsp;Verify that the office version includes local install. &nbsp;If there no local Word install, user can use https://products.office.com/en-us/business/compare-office-365-for-business-plans to view Office 365 version options)</li>
<li>If the user's Office 365 version includes a local install, do the following:</li>
</ol>
<p>A. START WITH uninstall and reinstall of station.&nbsp;</p>
<p>B. Verify the presence of PIAs (Microsoft &nbsp;) in C:\Windows\Assembly. &nbsp;Look for Microsoft.Office.Interop files</p>
<p>C. We have an installer for the PIAs that should put the items back O:\Tech Support\V8 PIA Installs. &nbsp;</p>
<p>D. &nbsp;If PIA install doesn't solve the error, &nbsp;suggest an Office Repair and explain the variables.</p>
<p>E. If you want to see the Registry entry, go to HKEY_CLASSES_ROOT\CSLID\{000209FF-0000-C000-000000000046}. We install these items when we install these Primary Interop Assemblies (PIAs) during the work station install. &nbsp;If the entry isn't listed, then something has removed it.</p>
<p>&nbsp;</p>