During SQL install folder C:\Windows\Temp failed

<p>Installing Microsoft SQL and you receive the following error: Updating permission setting for folder 'C:\Windows\Temp' failed. The folder permission setting were supposed to be set to 'D:(A;OICI;0x1300af;;;S-1-5-21-1376359789-4103010270-363085837-1034)'. We've seen this error twice in the past couple of weeks. The location after "supposed to be set to" was different both times.</p>

<ul style="padding-left: 60px;">
<li>You are presented with two options: retry, or cancel.</li>
<li>Retry doesn't work, same error.</li>
<li>Pressing cancel actually resumes the installation (you get the error about 80% through in the install).</li>
<li>The only feature that does not install is the "Reporting Services" which we don't use anyway.</li>
<li>All of the services are there (including reporting services)</li>
<li>I was able to install SU without issue; the database attaches just fine; AA launches and runs just fine.</li>
</ul>