CLIMARK in AAMobile

<h1>How to activate and use CLIMARK in AAMobile</h1>
<p>(Screenshots at O:\Tech Support\V8 Screens to Supplement TSL Entries\CLIMARK for AAMobile</p>
<p>&nbsp;</p>

<h2>NOTE: Screenshots at O:\Tech Support\V8 Screens to Supplement TSL Entries\CLIMARK for AAMobile</h2>
<p>&nbsp;</p>
<p>- in db, lookup name, copy key ID</p>
<p>- in Hosted Manager, select client, click Details</p>
<p>- click Enable CLIMARK Login</p>
<p>- message displays indicating how long CLIMARK is enabled</p>
<p>- go to Intranet</p>
<p>- under Mobile, click calendar icon, click Today (the field underneath the calendar field will asterisks ********)</p>
<p>- paste key ID in the blank field and click Get</p>
<p>- password will display, double-click to copy</p>
<p>- log into the user's AAMobile using CLIMARK and the above password&nbsp;</p>
<p>NOTE: Password is good all day for the database in which you enabled.&nbsp; The HM Details screen displays the time that CLIMARK will expire.&nbsp; After logging out of the user's AAMobile, if you need to log back into same AAMobile, in the HM Details click Enable CLIMARK again and then copy/paste the same password from the Intranet.</p>
<p>&nbsp;</p>