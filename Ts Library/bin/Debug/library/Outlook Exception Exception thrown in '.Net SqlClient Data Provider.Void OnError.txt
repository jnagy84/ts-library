Outlook Exception Exception thrown in '.Net SqlClient Data Provider.Void OnError

<p>AAOutlookAddin: version 17.71.671.2 AAOutlookAddIn.OutlookExportAACalendar.PrepareToProcess Exception Information Exception thrown in '.Net SqlClient Data Provider.Void OnError(System.Data.SqlClient.SqlException, Boolean, System.Action`1[System.Action])' Type: System.Data.SqlClient.SqlException ------------------------------ Message: A network-related or instance-specific error occurred while establishing a connection to SQL Server. The server was not found or was not accessible. Verify that the instance name is correct and that SQL Server is configured to allow remote connections. (provider: Named Pipes Provider, error: 40 - Could not open a connection to SQL Server) ------------------------------</p>

<h3>1) AA was moved to new server</h3>
<h3>2) AAOutlookAddin Registry entries are still pointing to old server</h3>
<h3>3) in the Registry, search for AAOutlookAddin7 (could be up to 4 entries)</h3>
<h3>4) Delete each AAOutlookAddin7 folder</h3>
<h3>5) Close Outlook</h3>
<h3>6) In AA, go to File | Preferences, make some kind of change in email archiving tab which will write new registry entries</h3>
<h3>7) Reopen Outlook</h3>
<h3>8) Test the sync</h3>