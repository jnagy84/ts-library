V8 Outlook Version and Registry Favorites

<p>In Outlook, go to OfficeAccount (in sidebar) and click About Outlook button on left.</p>
<p>&nbsp;</p>
<table class="MsoNormalTable" style="border-collapse: collapse; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 0in 0in 0in;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 13.5pt;">
<td style="width: 153.85pt; border: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center"><strong>Version Name</strong></p>
</td>
<td style="width: 153.85pt; border: solid windowtext 1.0pt; border-left: none; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center"><strong>Build Number</strong></p>
</td>
</tr>
<tr style="mso-yfti-irow: 1; height: 13.5pt;">
<td style="width: 153.85pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">&nbsp;</p>
</td>
<td style="width: 153.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">&nbsp;</p>
</td>
</tr>
<tr style="mso-yfti-irow: 2; height: 13.5pt;">
<td style="width: 153.85pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">Outlook 2003</p>
</td>
<td style="width: 153.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">11.0</p>
</td>
</tr>
<tr style="mso-yfti-irow: 3; height: 13.5pt;">
<td style="width: 153.85pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">Outlook 2007</p>
</td>
<td style="width: 153.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">12.0</p>
</td>
</tr>
<tr style="mso-yfti-irow: 4; height: 13.5pt;">
<td style="width: 153.85pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">Outlook 2010</p>
</td>
<td style="width: 153.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">14.0</p>
</td>
</tr>
<tr style="mso-yfti-irow: 5; height: 13.5pt;">
<td style="width: 153.85pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">Outlook 2013</p>
</td>
<td style="width: 153.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">15.0</p>
</td>
</tr>
<tr style="mso-yfti-irow: 6; mso-yfti-lastrow: yes; height: 13.5pt;">
<td style="width: 153.85pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">Outlook 2016</p>
</td>
<td style="width: 153.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 13.5pt;" valign="top" width="205">
<p class="MsoNormal" style="text-align: center;" align="center">16.0</p>
</td>
</tr>
</tbody>
</table>

<p class="MsoNormal">You can add registry key locations to your &ldquo;Favorites&rdquo; within the registry. I added all of the AAOutlookAddin locations that I could think of to my favorites, found the registry favorites subkey within my own registry, then exported it. This key can be imported into any registry with a simple double-click.</p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal">This will allow you to navigate to the &ldquo;Favorites&rdquo; section within the registry on a user&rsquo;s machine and quickly navigate to the various locations that the AAOutlookAddin7 subkey may reside.</p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal">O:\Pam\Jacob\ AAOutlookAddin Registry Favorites.reg</p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal">Try it on your own machine! You only need to search for the build number that corresponds to your version of Outlook (example: Search in the 15.0 folder for Outlook 2013 &ndash; see chart below)</p>
<p>&nbsp;</p>
<p class="MsoNormal">&nbsp;<strong>Registry key locations:</strong></p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal">HKEY_CURRENT_USER\SOFTWARE\Client Marketing Systems Inc.\Advisors Assistant\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Office\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\11.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\12.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\14.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\15.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\16.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Office\11.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Office\12.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Office\14.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Office\15.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Office\16.0\Outlook\Addins\AAOutlookAddin7</p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal"><!-- [if gte vml 1]><v:shapetype id="_x0000_t75" coordsize="21600,21600"
 o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f"
 stroked="f">
 <v:stroke joinstyle="miter"/>
 <v:formulas>
  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
  <v:f eqn="sum @0 1 0"/>
  <v:f eqn="sum 0 0 @1"/>
  <v:f eqn="prod @2 1 2"/>
  <v:f eqn="prod @3 21600 pixelWidth"/>
  <v:f eqn="prod @3 21600 pixelHeight"/>
  <v:f eqn="sum @0 0 1"/>
  <v:f eqn="prod @6 1 2"/>
  <v:f eqn="prod @7 21600 pixelWidth"/>
  <v:f eqn="sum @8 21600 0"/>
  <v:f eqn="prod @7 21600 pixelHeight"/>
  <v:f eqn="sum @10 21600 0"/>
 </v:formulas>
 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
 <o:lock v:ext="edit" aspectratio="t"/>
</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" alt="" style='width:594pt;
 height:295.5pt'>
 <v:imagedata src="file:///C:\Users\pam\AppData\Local\Temp\msohtmlclip1\01\clip_image001.png"
  o:href="cid:image003.png@01D213E6.AD40B2F0"/>
</v:shape><![endif]--><!-- [if !vml]--><img src="file:///C:\Users\pam\AppData\Local\Temp\msohtmlclip1\01\clip_image002.jpg" alt="" width="792" height="394" /><!--[endif]--></p>
<p class="MsoNormal">&nbsp;</p>