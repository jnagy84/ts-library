ORNSTO Error in Google Sync

<p>System.NullReferenceException: Object reference not set to an instance of an object.</p>
<p>&nbsp; &nbsp;at AAGoogle.AAGoogleImportExportProcessObj.GoogleExceptionHandler(Exception ee, Boolean&amp; exceptionYenc, String&amp; messageOut, Boolean writeToLog) in f:\V3Share\CMS\AA_Release\AAGoogle\AAGoogleImportExportProcessObj.cs:line 8065</p>
<p>&nbsp;</p>

<p class="MsoNormal">NATE says on 1/28/16:</p>
<p class="MsoNormal">It appears that they did not grant access to the Calendar API when they created their JSON file.&nbsp; They will need to go through the process of creating that file again, this time making sure to grant all the required permissions.</p>
<p class="MsoNormal">&nbsp;I&rsquo;ve changed the way Google error messages appear so that this type of problem will be much easier to diagnose in the future.</p>
<p class="MsoNormal">FWIW this is the actual error coming back from Google in Diener&rsquo;s case: <span style="color: red;">Access Not Configured. The API (Calendar API) is not enabled for your project. Please use the Google Developers Console to update your configuration. [403]</span></p>
<p class="MsoNormal">&nbsp;</p>