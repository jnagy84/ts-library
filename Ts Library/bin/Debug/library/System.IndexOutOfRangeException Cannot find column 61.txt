System.IndexOutOfRangeException Cannot find column 61

<p>User has a bad file in the download module's list of files to import.</p>

<h3>1) Open the module.</h3>
<h3>2) Right-click Import Files</h3>
<h3>3) Archive the problem file. (You might have to process one file at time if there's a batch so that you can locate the problem file.)</h3>