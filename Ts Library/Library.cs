﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ts_Library
{
    class Library
    {
        private string sItemdesc;
        public string sDescription{ get; set; }
    //private string ProblemResource;
    public Library(string sInfo)
        {
            sDescription = sInfo;

            int firstLine = sDescription.IndexOf("\r\n");

            sItemdesc = sDescription.Substring(0, firstLine);
            
        }
         public override string ToString() 
        {
            return sDescription;
        }
        public Boolean doesContain(string searchString)
        {
            if (sDescription.IndexOf(searchString, StringComparison.OrdinalIgnoreCase)!=-1)
            {
                return true;
            }
            else
            { 
                return false;
            }
        }
    }
}
