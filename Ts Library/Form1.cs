﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using Dapper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.IO;
using System.Data.SqlClient;

namespace Ts_Library
{
    public partial class Form1 : Form
    {
        //const String instancePath = "CMS-121\\ADVISORSASSIST;";
        const String instancePath = "CMS-116\\SQL2014;";

        const string Catalog = "Initial Catalog=Intranet;";

        const string loginCode = "Integrated Security=True;";

        SqlConnection sqlConn;

        Boolean newItem = false;

        sqlLibrary curLib;

        public Form1()
        {
            InitializeComponent();
        }

        public object ListBox1 { get; private set; }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetFontCollection();

            GetFontSizes();
            
            SQLConnect();

            SQLQuery(@"select * from dbo.Library where LibraryTypeID = 0  order by description asc");

            //Used for the Text Files version
            /*string sDirectory = $"{Application.StartupPath}\\Library\\";

            string[] fileList = Directory.GetFiles(sDirectory);

            Array.Resize(ref myLibrary, fileList.Length );

            for (int i = 0; i < fileList.Length ; i++)
            {
                string fileContents = File.ReadAllText(fileList[i]);

                myLibrary[i] = new Library(fileContents);

                listBox1.Items.Add(myLibrary[i]);
            }*/

        }
        private void SQLConnect()
        {
            try
            {
                sqlConn = new SqlConnection($"Data Source={instancePath}{Catalog}{loginCode}");

                this.Text = $"Connected to {sqlConn.DataSource}\\{sqlConn.Database} - Using Windows User {Environment.UserName}";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Connecting to SQL/r/n" + ex.Message + "/r/n" + ex.StackTrace);
            }
        }
        private void SQLQuery(String myQuery)
        {
            try
            {
                var items = sqlConn.Query<sqlLibrary>(myQuery, null, null, true, null, System.Data.CommandType.Text);

                foreach (sqlLibrary item in items)
                {
                    listBox1.Items.Add(item);
                }
                label1.Text = $"Search String: Returned {items.Count()} Results";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in Query/r/n" + ex.Message + "\r\n" + ex.StackTrace);
            }
            
        }
        private void SQLNonQuery(String myQuery)
        {
            try
            {
                sqlConn.Query(myQuery, null, null, true, null, System.Data.CommandType.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex==-1)
            {
                return;
            }

            curLib =(sqlLibrary) listBox1.SelectedItem;

            RTBWorker(rtbDescription, curLib.Description);

            RTBWorker(rtbProblem, curLib.ProblemResource);

            RTBWorker(rtbRes, curLib.Resolution);

            listBox1.Focus();
        }
        private void RTBWorker(RichTextBox rtb, string data)
        {
            rtb.SelectAll();

            rtb.SelectionBackColor = rtb.BackColor;

            string rtfHeader = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\nouicompat\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Calibri;}}{\\*\\generator Riched20 10.0.17763}\\viewkind4\\uc1\\pard\\sa200\\sl276\\slmult1\\f0\\fs22\\lang9 ";

            if (data.Length > 6 && data.Substring(0, 6) == "{\\rtf1")
            {
                rtb.Rtf = cleanFormating(data,chkFormating.Checked);
            }
            else
            {
                rtb.Rtf = rtfHeader + cleanFormating(addCRLF(data, chkFormating.Checked),chkFormating.Checked) + "}";
            }
            HighlightText(rtb, true);

        }
        private void HighlightText(RichTextBox rtb, Boolean highLight)
        {
            String[] searchString = textBox1.Text.Split(' ');

            for (int i = 0; i < searchString.Length; i++)
            {
                int searchLoc = rtb.Text.IndexOf(searchString[i], StringComparison.OrdinalIgnoreCase);

                while (searchLoc != -1)
                {
                    rtb.Select(searchLoc, searchString[i].Length);

                    if (highLight)
                    {
                        rtb.SelectionBackColor = Color.Yellow;
                    }
                    else
                    {
                        rtb.SelectionBackColor = rtb.BackColor;
                    }

                    if (searchLoc < rtb.Text.Length)
                    {
                        searchLoc = rtb.Text.IndexOf(searchString[i], searchLoc + 1, StringComparison.OrdinalIgnoreCase);
                    }
                    else
                    {
                        searchLoc = -1;
                    }
                }
            }
            rtb.Select(0, 0);
        }

        private string cleanFormating(string data, Boolean goOrNo)
        {
            if (goOrNo)
            {
                int startPoint = data.IndexOf("<");

                while (startPoint != -1) 
                {
                    int endPoint = data.IndexOf(">");

                    data = data.Substring(0,startPoint ) + data.Substring(endPoint+1);

                    startPoint =data.IndexOf("<");
                }
                data = removeText(data, "&nbsp;");
            }
            

            return data;
        }

        private string addCRLF(string data, Boolean goOrNo)
        {
            if (goOrNo)
            {
                int startPoint = data.IndexOf("\r\n");

                while (startPoint != -1)
                {
                    if (startPoint < 4 || data.Substring(startPoint - 4, 6) != "\\par\r\n")
                    {
                        data = data.Substring(0, startPoint) + "\\par\r\n" + data.Substring(startPoint + 2);
                    }
                    startPoint = data.IndexOf("\r\n", startPoint + 5);
                }
            }
            return data;
        }
        private string removeText(string data,string remove)
        {
            int startPoint = data.IndexOf(remove);

            while (startPoint != -1)
            {              
                data = data.Substring(0, startPoint) + data.Substring(startPoint + remove.Length );

                startPoint = data.IndexOf(remove);
            }
            return data;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            String[] searchKeys = textBox1.Text.Split(' ');

            string searchString="";

            foreach (string key in searchKeys)
            {
                if (key != "")
                {
                        searchString = searchString + $" and (description like '%{key}%' or ProblemResource like '%{key}%' or Resolution like '%{key}%')";
                }
            }

            SQLQuery($"select * from dbo.Library where LibraryTypeID = 0 {searchString} order by description asc");

            /* used for text version

            for (int i = 0;i<=myLibrary.length;i++)
            {
                Boolean bContain = true;

                for (int i2 =0; i2< searchString.Length;i2++)
                {
                    if (myLibrary[i].doesContain(searchString[i2])== false)
                    {
                        bContain = false;
                    }
                }
                if (bContain)
                {
                    listBox1.Items.Add(myLibrary[i]);
                }
            }*/
        }
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            rtbDescription.Text = "";

            rtbProblem.Text = "";

            rtbRes.Text = "";

            newItem = true;

            curLib = null;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (newItem)
            {
                SQLNonQuery($"INSERT INTO Library VALUES('{addApostrophe(rtbDescription.Text)}','','{addApostrophe(rtbProblem.Text)}','{addApostrophe(rtbRes.Text)}',0,'{Environment.UserName}',GETDATE())");

                newItem = false;
            }
            else if (curLib != null) 
            {
                HighlightText(rtbRes, false);

                SQLNonQuery($"update Library set description ='{addApostrophe(rtbDescription.Text)}', problemresource ='{addApostrophe(rtbProblem.Text)}',resolution ='{addApostrophe(rtbRes.Rtf)}',whochanged = '{Environment.UserName}', lastchanged = GETDATE() where ID = {curLib.ID}");

                textBox1_TextChanged(sender, e);
            }
            else
            {
                MessageBox.Show("Please select an Item");
            }
        }

        private string addApostrophe(string data)
        {
            int loc = data.IndexOf("'");

            while (loc != -1)
            {
                data = data.Insert(loc, "'");

                loc = data.IndexOf("'",loc + 2);
            }
            return data;
        }

        private void GetFontCollection()
        {
            InstalledFontCollection InsFonts = new InstalledFontCollection();

            foreach (FontFamily item in InsFonts.Families)
            {
                cmbFonts.Items.Add(item.Name);

                if (item.Name == rtbRes.Font.Name)
                {
                    cmbFonts.SelectedIndex = cmbFonts.Items.Count-1;
                }
            }
            //cmbFonts.SelectedIndex = 0;
        }

        private void GetFontSizes()
        {
            for (int i = 1; i <= 75; i++)
            {
                cmbFontSize.Items.Add(i);
            }

            cmbFontSize.SelectedIndex = 15;
        }

        public class sqlLibrary
        {
            public int ID { get; set; }
            public string Description { get; set; }
            public string MatchText { get; set; }
            public string ProblemResource { get; set; }
            public string Resolution { get; set; }
            public int LibraryTypeID { get; set; }
            public string WhoChanged { get; set; }
            public DateTime? LastChanged { get; set; }

            public override string ToString()
            {
                return Description;
            }
        }

        private void cmbFonts_SelectedIndexChanged(object sender, EventArgs e)
        {
            rtbRes.SelectionFont = new Font(cmbFonts.Text, rtbRes.SelectionFont.Size);

            rtbRes.Focus();
        }

        private void cmbFontSize_SelectedIndexChanged(object sender, EventArgs e)
        {        
            rtbRes.SelectionFont = new Font(rtbRes.SelectionFont.FontFamily,float.Parse(cmbFontSize.Text), new FontStyle());

            rtbRes.Focus();
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            DialogResult result =colorDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                rtbRes.SelectionColor = colorDialog1.Color;

                btnColor.BackColor = colorDialog1.Color;
            }
            rtbRes.Focus();
        }

        private void btnBold_Click(object sender, EventArgs e)
        {
            Boolean isBold =  !rtbRes.SelectionFont.Bold;

            if (isBold)
            {
                rtbRes.SelectionFont = new Font(rtbRes.SelectionFont.FontFamily, rtbRes.SelectionFont.Size,FontStyle.Bold);
            }
            else
            {
                rtbRes.SelectionFont = new Font(rtbRes.SelectionFont.FontFamily, rtbRes.SelectionFont.Size, new FontStyle());
            }
            rtbRes.Focus();
        }

        private void btnItal_Click(object sender, EventArgs e)
        {
            Boolean isItal = !rtbRes.SelectionFont.Italic;

            if (isItal)
            {
                rtbRes.SelectionFont = new Font(rtbRes.SelectionFont.FontFamily, rtbRes.SelectionFont.Size, FontStyle.Italic);
            }
            else
            {
                rtbRes.SelectionFont = new Font(rtbRes.SelectionFont.FontFamily, rtbRes.SelectionFont.Size, new FontStyle());
            }
            rtbRes.Focus();
        }

        private void btnUnder_Click(object sender, EventArgs e)
        {
            Boolean isUnderL = !rtbRes.SelectionFont.Underline;

            if (isUnderL)
            {
                rtbRes.SelectionFont = new Font(rtbRes.SelectionFont.FontFamily, rtbRes.SelectionFont.Size, FontStyle.Underline);
            }
            else
            {
                rtbRes.SelectionFont = new Font(rtbRes.SelectionFont.FontFamily, rtbRes.SelectionFont.Size, new FontStyle());
            }
            rtbRes.Focus();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            listBox1_SelectedIndexChanged(sender, e);
        }

        private void rtbRes_TextChanged(object sender, EventArgs e)
        {
            if (rtbRes.SelectionFont != null)
            {
                cmbFonts.Text = rtbRes.SelectionFont.Name;

                cmbFontSize.Text = rtbRes.SelectionFont.Size.ToString();

                btnColor.BackColor = rtbRes.SelectionColor;
            }
            
        }

        private void rtbRes_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(rtbRes,e.Location);
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            toolStripMenuItem3_Click(sender, e);

            rtbRes.SelectedText = "";
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(rtbRes.SelectedText);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
                rtbRes.Paste();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                listBox1.Focus();
            }
        }
    }
}
    
